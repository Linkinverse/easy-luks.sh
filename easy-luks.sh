#!/bin/bash

#you can replace this with "~" or "."
mountpoint="/mnt"

BOLDCOLOR="\e[91;2m"
ENDCOLOR="\e[0m"

if [[ $EUID -ne 0 ]]; then
   echo "You must be root to run this scriptt" 1>&2
   exit 1
fi

#checking for required packages
if dpkg -s ntfs-3g cryptsetup > /dev/null
then
  : # all ok, do nothing
else
  echo -e "${BOLDCOLOR}ERROR:${ENDCOLOR} Package to read NTFS or cryptsetup (or both) are missing. You should run: apt install ntfs-3g cryptsetup"
  exit 1
fi

#list mounted loopback devices and opened LUKS containers
if (( $# > 0 )) && [ $1 = "check" ]; then
  echo -e "${BOLDCOLOR}This script will execute the following 2 commands: ${ENDCOLOR}"
  echo "  losetup"
  echo "  lsblk | grep crypt"
  echo ""
  echo -e "${BOLDCOLOR}Check if a LUKS container is opened as a loopback device in /dev/mapper and the respective mountpoints in /$mountpoint ${ENDCOLOR}"
  /sbin/losetup
  echo -e "\n${BOLDCOLOR}Mounted devices: ${ENDCOLOR}"
  lsblk | grep crypt
  exit 0
fi

if (( $# < 2 )); then
   echo "Not enough arguments."
   echo -e "\nUsage:"
   echo "  $0  create | open | close | check  <container_filename>"
   exit 1
fi

if [ ! $1 = "create" ] && [ ! $1 = "open" ] && [ ! $1 = "close" ]; then
  echo -e "\nCommand ${BOLDCOLOR}$1${ENDCOLOR} not recognised."
  echo -e "Please use parameter ${BOLDCOLOR}create${ENDCOLOR}, ${BOLDCOLOR}open${ENDCOLOR}, ${BOLDCOLOR}close${ENDCOLOR}, or ${BOLDCOLOR}check${ENDCOLOR} \n"
  exit 1
fi

volumename=$2

# ----------------------------------------------------------------------

echo -e "Mountpoint is currently set to: ${BOLDCOLOR}$mountpoint ${ENDCOLOR}"
echo -e "Executing operation: ${BOLDCOLOR}$1${ENDCOLOR} on file ${BOLDCOLOR}$volumename ${ENDCOLOR}\n"

#creates a new file
if [ $1 = "create" ]; then
  #basename extracts the file on a path, perfect to erase the base url and keep the filename. it also doesnt care for the http string
  #sed regexp will remove any special character from the filename for the mountpoint
  volumebasename=$(basename "$volumename-Mount" | sed 's/[^a-zA-Z0-9]//g')

  read -p "Enter volume size (in Megabytes): " volumesize
  echo ""
  echo -e "${BOLDCOLOR}This script will execute the following 5 commands: ${ENDCOLOR}"
  echo "  dd if=/dev/urandom of=$volumename bs=1M count=$volumesize"
  echo "  cryptsetup -y luksFormat $volumename"
  echo "  cryptsetup luksOpen $volumename $volumebasename"
  echo "  mkfs.ext4 -j /dev/mapper/$volumebasename"
  echo "  cryptsetup luksClose $volumebasename"
  echo ""
  read -p "Press enter to start..."
  echo -e "\n${BOLDCOLOR}Creating file $volumename of $volumesize Mb, and filling it with random data. This will take a while, please wait... ${ENDCOLOR}"
  dd if=/dev/urandom of=$volumename bs=1M count=$volumesize
  echo -e "Done.\n------------------------------\n"
  echo -e "${BOLDCOLOR}Initializing container $volumename with cryptsetup... ${ENDCOLOR}"
  /sbin/cryptsetup -y luksFormat $volumename
  echo -e "Done.\n------------------------------\n"
  echo -e "${BOLDCOLOR}Opening empty container $volumename with LUKS to create a new filesystem... ${ENDCOLOR}"
  /sbin/cryptsetup luksOpen $volumename $volumebasename
  echo -e "Done.\n------------------------------\n"
  echo -e "${BOLDCOLOR}Check if container $volumename is now opened as loopback device in /dev/mapper/$volumebasename ${ENDCOLOR}\n"
  /sbin/losetup
  echo ""
  lsblk
  echo ""
  read -p "Press enter to prepare the new ext4 filesystem..."
  echo -e "\n${BOLDCOLOR}Preparing filesystem ext4 for /dev/mapper/$volumebasename... ${ENDCOLOR}"
  /sbin/mkfs.ext4 -j /dev/mapper/$volumebasename
  echo -e "Done.\n------------------------------\n"
  echo -e "${BOLDCOLOR}Closing /dev/mapper/$volumebasename... ${ENDCOLOR}"
  /sbin/cryptsetup luksClose $volumebasename
  echo -e "Done.\n------------------------------\n"
  echo -e "Encypted LUKS volume created successfully into portable container $volumename.\n"
fi

# ----------------------------------------------------------------------

#open and mounts crypted loopback file
if [ $1 = "open" ]; then

  if [ ! -f "$volumename" ]; then
    echo "File container $volumename does not exists."
    exit 1
  fi
  #basename extracts the file on a path, perfect to erase the base url and keep the filename. it also doesnt care for the http string
  #sed regexp will remove any special character from the filename for the mountpoint
  volumebasename=$(basename "$volumename-Mount" | sed 's/[^a-zA-Z0-9]//g')

  echo -e "What kind of container do you want to open?"
  echo -e " [${BOLDCOLOR}1${ENDCOLOR}] for LUKS1,\n [${BOLDCOLOR}2${ENDCOLOR}] for LUKS2,"
  echo -e " [${BOLDCOLOR}T${ENDCOLOR}] for TrueCrypt/VeraCrypt,\n [${BOLDCOLOR}A${ENDCOLOR}] for loopaes"
  read -n 1 -p "[1/2/T/A]: " ans;
  case $ans in
    1)
      containertype="--type luks1 luksOpen";;
    2)
      containertype="--type luks2 luksOpen";;
    t|T)
      containertype="--type tcrypt open";;
    a|A)
      containertype="--type loopaes open";;
    b|B)
      containertype="--type bitlk open";;
    *)
      exit;;
  esac
  echo -e "\nOk."
  while true; do
    read -n 1 -p "Do you want to mount as readonly? [Y/N]: " yn
    case $yn in
        [Yy]* ) readonlyflag="--readonly"; echo -e "\nWill mount as read only.";break;;
        [Nn]* ) readonlyflag=""; echo -e "\nWill mount as read/write.";break;;
        * ) echo -e "\nPlease press [Y] or [N].\n";;
    esac
  done
  echo -e "\n${BOLDCOLOR}This script will execute the following 2 commands: ${ENDCOLOR}"
  echo "  cryptsetup $readonlyflag $containertype $volumename $volumebasename"
  echo "  mount /dev/mapper/$volumebasename $mountpoint/$volumebasename"
  echo ""

  echo -e "${BOLDCOLOR}Opening volume $volumename as loopback device: /dev/mapper/$volumebasename ${ENDCOLOR}"
  /sbin/cryptsetup $readonlyflag $containertype "$volumename" "$volumebasename" && echo -e "Success. \n" || echo -e "ERROR OPENING VOLUME! \n"

  #create folder only if doesnt already exist
  [ -d "$mountpoint/$volumebasename" ] || mkdir "$mountpoint/$volumebasename"

  echo -e "${BOLDCOLOR}Mounting loopback device /dev/mapper/$volumebasename to $mountpoint/$volumebasename ${ENDCOLOR}"
  mount "/dev/mapper/$volumebasename" "$mountpoint/$volumebasename" && echo -e "--> SUCCESS! \n" || echo -e "--> ERROR MOUNTING! \n"
  echo -e "To check if container ${BOLDCOLOR}$volumename${ENDCOLOR} is now opened as loopback device in ${BOLDCOLOR}/dev/mapper/$volumebasename${ENDCOLOR} and check respective mountpoint into ${BOLDCOLOR}$mountpoint/$volumebasename${ENDCOLOR} type: \n"
  echo -e "  $0 check \n"

# python file server, uncomment if necessary
#  while true; do
#    read -n 1 -p "If mounted corretly, do you want to serve the folder as HTTP? [Y/N]: " yn
#    case $yn in
#        [Yy]* ) echo -e "\nServing folder as HTTP on port 8000 for easy access... (press CTRL+C to close)"; python3 -m http.server --directory $mountpoint/$volumebasename;break;;
#        [Nn]* ) echo "";break;;
#        * ) echo -e "\nPlease press [Y] or [N].\n";;
#    esac
#  done
fi

# ------------------------------------------------------------------------

#unmounts and close previously mounted loopback file
if [ $1 = "close" ]; then
  echo "--------"
  #basename extracts the file on a path, perfect to erase the base url and keep the filename. it also doesnt care for the http string
  #sed regexp will remove any special character from the filename for the mountpoint
  if [ ! -d "$volumename" ]; then
    echo "Directory does not exist: $volumename"
    echo "Maybe parameter is referring to the container filename, not the mountpoint? Checking..."
    if [ ! -f "$volumename" ]; then
      echo "File container does not exist: $volumename"
      echo "Nothing found that could be unmounted, please double-check parameters. Exiting."
      exit 1
    else
      echo "File container found: $volumename"
      volumebasename=$(basename "$volumename-Mount" | sed 's/[^a-zA-Z0-9]//g')
      echo "Then the mountpoint is supposed to be: $mountpoint/$volumebasename"
    fi
  else
    echo "Directory found: $volumename"
    volumebasename=$(basename "$volumename" | sed 's/[^a-zA-Z0-9]//g')
    echo "Then the mountpoint is supposed to be: $mountpoint/$volumebasename"
  fi

  echo ""
  echo -e "${BOLDCOLOR}This script will execute the following 2 commands: ${ENDCOLOR}"
  echo "  umount $mountpoint/$volumebasename"
  echo "  cryptsetup close $volumename"
  echo ""

  echo -e "${BOLDCOLOR}Unmounting loopback device /dev/mapper/$volumebasename from $mountpoint/$volumebasename ${ENDCOLOR}"
  umount $mountpoint/"$volumebasename" && echo -e "Success. \n" || echo -e "ERROR UNMOUNTING! \n"
  rmdir $mountpoint/"$volumebasename"

  echo -e "${BOLDCOLOR}Closing volume $volumename from loopback device: /dev/mapper/$volumebasename ${ENDCOLOR}"
  /sbin/cryptsetup close "/dev/mapper/$volumebasename" && echo -e "--> SUCCESS! \n" || echo -e "--> ERROR CLOSING! \n"

  echo -e "To check if container ${BOLDCOLOR}$volumename${ENDCOLOR} has been closed from loopback device ${BOLDCOLOR}/dev/mapper/$volumebasename${ENDCOLOR} and verify it is not mounted anymore on ${BOLDCOLOR}$mountpoint/$volumebasename${ENDCOLOR} type: \n"
  echo -e "  $0 check \n"
fi
