## Create, mount, and unmount your LUKS or TrueCrypt/VeraCrypt containers, and optionally make them available from any device in your LAN/Wi-Fi

A wrapper around basically the following few commands:

**1. Create an encrypted container:**
```
dd if=/dev/urandom of=$volumename bs=1M count=$volumesize
cryptsetup -y luksFormat $volumename
cryptsetup luksOpen $volumename $volumebasename
mkfs.ext4 -j /dev/mapper/$volumebasename
cryptsetup luksClose $volumebasename
```

**2. Open and mount an encrypted container:**
```
cryptsetup luksOpen $volumename $volumebasename
mount /dev/mapper/$volumebasename /mnt/$volumebasename
```

**3. Unmount and close an encrypted container:**
```
umount /mnt/$volumebasename
cryptsetup luksClose $volumename
```

![](screenshot.jpg)

**Why:**

Useful for anyone not familiar with dm-crypts / cryptsetup / LUKS / loopback devices.

Or to anyone who wants a simpler, handy and portable alternative to VeraCrypt or dyne.org/tomb (since they cannot be mounted on Android).

Or to anyone who needs to access an encrypted containers mounting it directly from a LAN device to a shared folder (i.e.: connect to your NAS with SSH from your phone, and temporarily mount the archive to a folder shared on LAN with SMB/NFS).

**Features:**

- You can open and access remote TrueCrypt/VeraCrypt and LUKS containers, using any SSH client (in the following screenshot I used OpenSSH with [Termux](https://termux.com) directly from an Android mobile phone)
- Even though you can simply mount the content of your container into the existing SMB share, you can now access it directly from any devices connected to the LAN, just using any browser (in the screenshot I used Chrome from Android)

![](termux-chrome.jpg)

**Requirements:**

- `apt-get install cryptsetup`
- Any Linux machine with root access, where you can mount the containers, i.e.: a NAS with SSH access, a Raspberry Pi, or [Andronix](https://play.google.com/store/apps/details?id=studio.com.techriz.andronix) installed on your Android phone (no need to root your phone), a virtual machine, etc.
